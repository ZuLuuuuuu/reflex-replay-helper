﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using System.Diagnostics;

namespace ReflexReplayHelper
{
    public class ReplayViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private App app;
        private Replay replay;
        private bool downloading;
        private bool downloaded;
        private double progressPercent;

        public IList<Player> Players
        {
            get
            {
                return Replay.Players;
            }
        }

        public string Map
        {
            get
            {
                return replay.Map;
            }

            set
            {
                replay.Map = value;

                NotifyPropertyChanged(nameof(Map));
            }
        }

        public string GameMode
        {
            get
            {
                return replay.GameMode;
            }

            set
            {
                replay.GameMode = value;

                NotifyPropertyChanged(nameof(GameMode));
            }
        }

        public uint MarkerCount
        {
            get
            {
                return replay.MarkerCount;
            }

            set
            {
                replay.MarkerCount = value;

                NotifyPropertyChanged(nameof(MarkerCount));
            }
        }

        public string MatchTitle
        {
            get
            {
                return Replay.MatchTitle;
            }
        }

        public string FileName
        {
            get
            {
                return replay.FileName;
            }

            set
            {
                replay.FileName = value;

                NotifyPropertyChanged(nameof(FileName));
            }
        }

        public string Url
        {
            get
            {
                return replay.Url;
            }

            set
            {
                replay.Url = value;

                NotifyPropertyChanged(nameof(Url));
            }
        }

        public bool Downloaded
        {
            get
            {
                return downloaded;
            }

            set
            {
                downloaded = value;

                NotifyPropertyChanged(nameof(Downloaded));
                NotifyPropertyChanged(nameof(IsDownloadButtonEnabled));
                NotifyPropertyChanged(nameof(PlayButtonToolTip));
            }
        }

        public bool Downloading
        {
            get
            {
                return downloading;
            }

            set
            {
                downloading = value;

                NotifyPropertyChanged(nameof(Downloading));
                NotifyPropertyChanged(nameof(IsDownloadButtonEnabled));
                NotifyPropertyChanged(nameof(PlayButtonToolTip));
            }
        }

        public double ProgressPercent
        {
            get
            {
                return progressPercent;
            }

            set
            {
                progressPercent = value;

                NotifyPropertyChanged(nameof(ProgressPercent));
            }
        }

        public bool IsDownloadButtonEnabled
        {
            get
            {
                return !Downloading && !Downloaded;
            }
        }

        public string PlayButtonToolTip
        {
            get
            {
                if (Downloaded)
                    return "Play Replay";
                else
                    return "Download and Play Replay";
            }
        }

        public Replay Replay
        {
            get
            {
                return replay;
            }
        }

        public ReplayViewModel(Replay replay)
        {
            this.app = (App)App.Current;
            this.replay = replay;
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public static string ReplayFileNameFromUrl(string url)
        {
            return url.Split('/').Last();
        }

        public async Task Download()
        {
            var client = new HttpClient();

            Downloading = true;

            using (HttpResponseMessage response = await client.GetAsync(Url, HttpCompletionOption.ResponseHeadersRead))
            {
                if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    Downloading = false;
                    throw new ReplayNotReadyForDownloadException();
                }

                long? totalFileSize = null;

                if (response.Content.Headers.ContentLength.HasValue)
                {
                    totalFileSize = response.Content.Headers.ContentLength.Value;
                }

                var replayFileName = ReplayViewModel.ReplayFileNameFromUrl(Url);
                var replayFolderPath = Path.Combine(app.ReflexFolderPath, "replays");
                if (!Directory.Exists(replayFolderPath))
                {
                    Directory.CreateDirectory(replayFolderPath);
                }

                var replayFilePath = Path.Combine(replayFolderPath, replayFileName);

                using (var streamToWriteTo = File.Open(replayFilePath, FileMode.Create, FileAccess.Write))
                {
                    using (var stream = await response.Content.ReadAsStreamAsync())
                    {
                        var bufferLength = 10000;
                        var buffer = new byte[bufferLength];
                        int bytesRead = 0;
                        long totalRead = 0;
                        while ((bytesRead = await stream.ReadAsync(buffer, 0, bufferLength)) > 0)
                        {
                            await streamToWriteTo.WriteAsync(buffer, 0, bytesRead);
                            totalRead += bytesRead;

                            if (totalFileSize != null)
                            {
                                ProgressPercent = totalRead * 100.0 / (long)totalFileSize;
                            }
                        }
                    }
                }

                FileName = replayFileName;

                Downloaded = true;
            }

            Downloading = false;
        }

        public void Play()
        {
            var fileNameWithoutExt = Path.GetFileNameWithoutExtension(FileName);

            var pInfo = new ProcessStartInfo("reflex.exe");
            pInfo.WorkingDirectory = app.ReflexFolderPath;
            pInfo.Arguments = @"+play " + fileNameWithoutExt;

            Process.Start(pInfo);
        }

        public void DeleteFile()
        {
            var fullFilePath = Path.Combine(app.ReflexFolderPath, "replays");

            if (!string.IsNullOrEmpty(FileName))
            {
                fullFilePath = Path.Combine(fullFilePath, FileName);
            }
            else if (!string.IsNullOrEmpty(Url))
            {
                fullFilePath = Path.Combine(fullFilePath, ReplayViewModel.ReplayFileNameFromUrl(Url));
            }
            else
            {
                return;
            }

            if (File.Exists(fullFilePath))
            {
                File.Delete(fullFilePath);
            }
        }
    }
}
