﻿using AngleSharp.Parser.Html;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;

namespace ReflexReplayHelper
{
    public partial class App : Application
    {
        public string ReflexFolderPath
        {
            get
            {
                return ReflexReplayHelper.Properties.Settings.Default.ReflexFolderPath;
            }

            set
            {
                ReflexReplayHelper.Properties.Settings.Default.ReflexFolderPath = value;
                ReflexReplayHelper.Properties.Settings.Default.Save();
            }
        }

        public string FilterSteamId
        {
            get
            {
                return ReflexReplayHelper.Properties.Settings.Default.FilterSteamId;
            }

            set
            {
                ReflexReplayHelper.Properties.Settings.Default.FilterSteamId = value;
                ReflexReplayHelper.Properties.Settings.Default.Save();
            }
        }

        public App()
        {
            ReflexFolderPath = ReflexReplayHelper.Properties.Settings.Default.ReflexFolderPath;
            FilterSteamId = ReflexReplayHelper.Properties.Settings.Default.FilterSteamId;
        }

        public async Task<IList<Replay>> GetRecentOnlineReplays()
        {
            var replays = new List<Replay>();
            
            var client = new HttpClient();

            var recentReplaysUrl = "http://reflexmm-149202.appspot.com/matchhistory";
            if (!string.IsNullOrEmpty(FilterSteamId.Trim()))
            {
                recentReplaysUrl = recentReplaysUrl + "?steamId=" + Uri.EscapeUriString(FilterSteamId.Trim());
            }

            HttpResponseMessage response = await client.GetAsync(recentReplaysUrl);
            string responseContent = await response.Content.ReadAsStringAsync();

            var htmlParser = new HtmlParser();
            var doc = await htmlParser.ParseAsync(responseContent);

            var matchHistoryElements = doc.QuerySelectorAll("table#matchhistory");
            foreach (var matchHistoryElement in matchHistoryElements)
            {
                var replay = new Replay();

                var headerInfoAndTeamsRows = matchHistoryElement.QuerySelectorAll("tr");
                
                if (headerInfoAndTeamsRows.Length >= 2)
                {
                    var matchHeaderInfoElements = headerInfoAndTeamsRows[0].QuerySelectorAll("th");

                    if (matchHeaderInfoElements.Length == 2)
                    {
                        var typeMapTimeElement = matchHeaderInfoElements[0];

                        var typeMapTimeString = typeMapTimeElement.TextContent.Trim();

                        var typeAndRest = typeMapTimeString.Split(':');
                        if (typeAndRest.Length == 2)
                        {
                            var type = typeAndRest[0].Trim();
                            replay.GameMode = type;

                            var mapAndTime = typeAndRest[1].Split('-');

                            if (mapAndTime.Length == 2)
                            {
                                replay.Map = mapAndTime[0].Trim();
                            }
                        }

                        var statsAndDownloadElements = matchHeaderInfoElements[1].QuerySelectorAll("a");
                        if (statsAndDownloadElements.Length == 2)
                        {
                            var downloadElement = statsAndDownloadElements[1];
                            replay.Url = downloadElement.Attributes["href"].Value;
                        }
                    }

                    for (var i = 1; i < headerInfoAndTeamsRows.Length; i++)
                    {
                        var teamsRow = headerInfoAndTeamsRows[i].QuerySelectorAll("td");
                        if (teamsRow.Length == 6)
                        {
                            var player1 = new Player();
                            var player2 = new Player();
                            
                            var team1PlayerInfoElement = teamsRow[0];
                            var team1PlayerStatsElement = teamsRow[1];
                            var team1PlayerScoreElement = teamsRow[2];

                            var team2PlayerInfoElement = teamsRow[5];
                            var team2PlayerStatsElement = teamsRow[4];
                            var team2PlayerScoreElement = teamsRow[3];

                            if (team1PlayerInfoElement.ChildElementCount > 0)
                            {
                                var team1PlayerNameElement = team1PlayerInfoElement.QuerySelector("a");
                                if (team1PlayerNameElement != null)
                                {
                                    player1.Name = team1PlayerNameElement.TextContent.Trim();
                                    player1.Team = "A";
                                    replay.Players.Add(player1);
                                }
                            }

                            if (team2PlayerInfoElement.ChildElementCount > 0)
                            {
                                var team2PlayerNameElement = team2PlayerInfoElement.QuerySelector("a");
                                if (team2PlayerNameElement != null)
                                {
                                    player2.Name = team2PlayerNameElement.TextContent.Trim();
                                    player2.Team = "B";
                                    replay.Players.Add(player2);
                                }
                            }
                        }
                    }

                    replays.Add(replay);
                }
            }

            return replays;
        }

        public IList<Replay> GetDownloadedReplays(ReplayOrderType order=ReplayOrderType.DateDescending)
        {
            var downloadedReplays = new List<Replay>();

            var replaysFolder = Path.Combine(ReflexFolderPath, "replays");

            if (Directory.Exists(replaysFolder))
            {
                var directory = new DirectoryInfo(replaysFolder);
                var replayFiles = directory.GetFiles("*.rep");

                IEnumerable<FileInfo> orderedReplayFiles;

                if (order == ReplayOrderType.DateAscending)
                {
                    orderedReplayFiles = replayFiles.OrderBy(f => f.LastWriteTime);
                }
                else if (order == ReplayOrderType.AlphabeticalDescending)
                {
                    orderedReplayFiles = replayFiles.OrderByDescending(f => Path.GetFileName(f.FullName));
                }
                else if (order == ReplayOrderType.AlphabeticalAscending)
                {
                    orderedReplayFiles = replayFiles.OrderBy(f => Path.GetFileName(f.FullName));
                }
                else
                {
                    orderedReplayFiles = replayFiles.OrderByDescending(f => f.LastWriteTime);
                }

                foreach (var replayFile in orderedReplayFiles)
                {
                    try
                    {
                        var replay = Utilities.ReplayFromFilePath(replayFile.FullName);
                        downloadedReplays.Add(replay);
                    }
                    catch (IOException)
                    { }
                }
            }

            return downloadedReplays;
        }

        public static bool IsValidReflexFolderPath(string folderPath)
        {
            var result = false;

            if (Directory.Exists(folderPath))
            {
                var directoryFiles = Directory.GetFiles(folderPath);

                if (directoryFiles.Contains(Path.Combine(folderPath, "reflex.exe")))
                {
                    result = true;
                }
            }

            return result;
        }

        public static void ShowInvalidReflexFolderWarning()
        {
            MessageBox.Show("Please first select a valid reflex folder.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public async static Task<IList<string>> DownloadLatest1000ReplayURLs()
        {
            var replayUrls = new List<string>();

            var client = new HttpClient();

            var last1000ReplaysUrl = "http://replays.reflexarena.com/latest1000/";

            HttpResponseMessage response = await client.GetAsync(last1000ReplaysUrl);
            string responseContent = await response.Content.ReadAsStringAsync();

            var htmlParser = new HtmlParser();
            var doc = await htmlParser.ParseAsync(responseContent);

            var replayElements = doc.QuerySelectorAll("div.list table tbody tr");
            foreach (var replayElement in replayElements)
            {
                var linkElement = replayElement.QuerySelector("td.n a");
                replayUrls.Add(linkElement.Attributes["href"].Value);
                //Console.WriteLine(linkElement.Attributes["href"].Value);
            }

            return replayUrls;
        }

        public async static Task DownloadReplayData(string url, IList<Replay> replayData)
        {
            var client = new HttpClient();
            using (HttpResponseMessage response = await client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead))
            {
                using (var stream = await response.Content.ReadAsStreamAsync())
                {
                    var replay = Utilities.ReplayFromStream(stream);
                    replayData.Add(replay);
                }
            }
        }

        public async static Task<IList<Replay>> DownloadLatest1000ReplayData()
        {
            var latest1000ReplayData = new List<Replay>();

            var last1000ReplayUrls = await App.DownloadLatest1000ReplayURLs();
            last1000ReplayUrls = last1000ReplayUrls.Take(50).ToList();

            var downloadTasks = new List<Task>();

            foreach (var replayUrl in last1000ReplayUrls)
            {
                Task fooWrappedInTask = Task.Run(() => DownloadReplayData(replayUrl, latest1000ReplayData));
                downloadTasks.Add(fooWrappedInTask);
            }

            Task.WaitAll(downloadTasks.ToArray());

            return latest1000ReplayData;
        }
    }
}
