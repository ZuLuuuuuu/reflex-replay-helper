﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflexReplayHelper
{
    public class Replay
    {
        IList<Player> players;
        string map;
        string gameMode;
        uint markerCount;
        string fileName;
        string url;

        public IList<Player> Players
        {
            get
            {
                return players;
            }
        }

        public string Map
        {
            get
            {
                return map;
            }

            set
            {
                map = value;
            }
        }

        public string GameMode
        {
            get
            {
                return gameMode;
            }

            set
            {
                gameMode = value;
            }
        }

        public uint MarkerCount
        {
            get
            {
                return markerCount;
            }

            set
            {
                markerCount = value;
            }
        }

        public string FileName
        {
            get
            {
                return fileName;
            }

            set
            {
                fileName = value;
            }
        }

        public string Url
        {
            get
            {
                return url;
            }

            set
            {
                url = value;
            }
        }

        public string MatchTitle
        {
            get
            {
                var teamAPlayers = Players.Where(item => item.Team == "A").Select(item => item.Name);
                var teamBPlayers = Players.Where(item => item.Team == "B").Select(item => item.Name);

                var representation = "";

                if (teamBPlayers.Count() == 0)
                {
                    representation = string.Join(" vs ", teamAPlayers);
                }
                else
                {
                    representation = string.Join(", ", teamAPlayers) + " vs " + string.Join(", ", teamBPlayers);
                }

                if (!string.IsNullOrEmpty(Map))
                {
                    representation = representation + " @ " + Map;
                }

                return representation;
            }
        }

        public Replay()
        {
            players = new List<Player>();
        }
    }
}
