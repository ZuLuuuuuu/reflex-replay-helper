﻿#define REPLAY_TAG 0xD00D001D
#define SERVER_MAX_CLIENTS 16

struct ReplayHeaderPlayer
{
    char name[32];
    int32_t score;
    int32_t team;
    uint64_t steamId;
};

struct ReplayHeader
{
    uint32_t tag;
    uint32_t protocolVersion;
    uint32_t playerCount;
    uint32_t markerCount;
    uint32_t gameFinished;    // i.e. got to end game screen    THIS IS NOT ALWAYS PRESENT IN 0x3F
                            // 32 bits padding here
    uint32_t padding;        // SHUT UP GCC

    uint64_t workshopId;    // if workshop map, this is the id for it
    uint64_t epochStartTime;
    char     szGameMode[64];
    char     szMapName[256];
    char     szHostName[256];
    struct ReplayHeaderPlayer    players[SERVER_MAX_CLIENTS];
};