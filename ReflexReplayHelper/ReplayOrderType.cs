﻿using System.Windows;

namespace ReflexReplayHelper
{
    public partial class App : Application
    {
        public enum ReplayOrderType
        {
            DateDescending,
            DateAscending,
            AlphabeticalDescending,
            AlphabeticalAscending
        }
    }
}
