﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Ookii.Dialogs.Wpf;

namespace ReflexReplayHelper
{
    public partial class MainWindow : Window
    {
        public IList<string> commonReflexFolderPaths = new List<string>() { @"C:\Program Files (x86)\Steam\steamapps\common\reflexfps",
                                                                            @"C:\Program Files\Steam\steamapps\common\reflexfps",
                                                                            @"D:\Program Files (x86)\Steam\steamapps\common\reflexfps",
                                                                            @"D:\Program Files\Steam\steamapps\common\reflexfps",
                                                                            @"C:\SteamLibrary\steamapps\common\reflexfps",
                                                                            @"D:\SteamLibrary\steamapps\common\reflexfps",
                                                                            @"E:\SteamLibrary\steamapps\common\reflexfps",
                                                                            @"F:\SteamLibrary\steamapps\common\reflexfps",
                                                                            @"G:\SteamLibrary\steamapps\common\reflexfps",
                                                                            @"H:\SteamLibrary\steamapps\common\reflexfps"};

        App app;
        MainWindowViewModel viewModel;

        public MainWindow()
        {
            app = (App)App.Current;

            InitializeComponent();

            viewModel = new MainWindowViewModel();
            DataContext = viewModel;
        }

        private async void DownloadRecentOnlineReplayButton_Click(object sender, RoutedEventArgs e)
        {
            if (!App.IsValidReflexFolderPath(app.ReflexFolderPath))
            {
                App.ShowInvalidReflexFolderWarning();
                return;
            }

            Button button = (Button)sender;
            ReplayViewModel viewModel = (ReplayViewModel)button.DataContext;

            try
            {
                await viewModel.Download();

                RefreshDownloadedReplays();
            }
            catch (ReplayNotReadyForDownloadException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async void PlayRecentOnlineReplayButton_Click(object sender, RoutedEventArgs e)
        {
            if (!App.IsValidReflexFolderPath(app.ReflexFolderPath))
            {
                App.ShowInvalidReflexFolderWarning();
                return;
            }

            Button button = (Button)sender;
            ReplayViewModel viewModel = (ReplayViewModel)button.DataContext;

            if (viewModel.Downloaded)
            {
                viewModel.Play();
            }
            else
            {
                try
                {
                    await viewModel.Download();
                    RefreshDownloadedReplays();
                    viewModel.Play();
                }
                catch (ReplayNotReadyForDownloadException ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void PlayDownloadedReplayButton_Click(object sender, RoutedEventArgs e)
        {
            if (!App.IsValidReflexFolderPath(app.ReflexFolderPath))
            {
                App.ShowInvalidReflexFolderWarning();
                return;
            }

            Button button = (Button)sender;
            ReplayViewModel viewModel = (ReplayViewModel)button.DataContext;
            viewModel.Play();
        }

        private void RefreshDownloadedReplays()
        {
            viewModel.RefreshDownloadedReplays();

            DownloadedReplaysScrollViewer.ScrollToTop();
        }

        private async Task RefreshRecentOnlineReplays()
        {
            await viewModel.RefreshRecentOnlineReplays();

            RecentOnlineReplaysScrollViewer.ScrollToTop();
        }

        private void RefreshDownloadedReplaysButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshDownloadedReplays();
        }

        private async void RefreshRecentOnlineReplaysButton_Click(object sender, RoutedEventArgs e)
        {
            await RefreshRecentOnlineReplays();
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!App.IsValidReflexFolderPath(viewModel.ReflexFolderPath))
            {
                foreach (var folderPathCandidate in commonReflexFolderPaths)
                {
                    if (App.IsValidReflexFolderPath(folderPathCandidate))
                    {
                        viewModel.ReflexFolderPath = folderPathCandidate;
                        break;
                    }
                }
            }
            
            viewModel.RefreshDownloadedReplays();
            await viewModel.RefreshRecentOnlineReplays();
        }

        private void DeleteRecentOnlineReplayButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete this replay?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                Button button = (Button)sender;
                ReplayViewModel replayViewModel = (ReplayViewModel)button.DataContext;
                replayViewModel.DeleteFile();

                replayViewModel.Downloaded = false;

                viewModel.RefreshDownloadedReplays();
            }
        }

        private void DeleteDownloadedReplayButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete this replay?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                Button button = (Button)sender;
                ReplayViewModel replayViewModel = (ReplayViewModel)button.DataContext;
                replayViewModel.DeleteFile();

                viewModel.DownloadedReplays.Remove(replayViewModel);
            }
        }

        private void DownloadedReplaysOrderComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (viewModel == null)
            //    return;

            //if (DownloadedReplaysOrderComboBox.SelectedIndex == 1)
            //{
            //    viewModel.DownloadedReplaysOrder = App.ReplayOrderType.DateAscending;
            //}
            //else if (DownloadedReplaysOrderComboBox.SelectedIndex == 2)
            //{
            //    viewModel.DownloadedReplaysOrder = App.ReplayOrderType.AlphabeticalAscending;
            //}
            //else if (DownloadedReplaysOrderComboBox.SelectedIndex == 3)
            //{
            //    viewModel.DownloadedReplaysOrder = App.ReplayOrderType.AlphabeticalDescending;
            //}
            //else
            //{
            //    viewModel.DownloadedReplaysOrder = App.ReplayOrderType.DateDescending;
            //}
        }

        private void SelectReflexFolderButton_Click(object sender, RoutedEventArgs e)
        {
            var fileDialog = new Microsoft.Win32.OpenFileDialog();
            var folderDialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();

            if (Directory.Exists(viewModel.ReflexFolderPath))
            {
                folderDialog.SelectedPath = viewModel.ReflexFolderPath;
            }
            else
            {
                folderDialog.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }

            var result = folderDialog.ShowDialog(this);
            if ((bool)result == true)
            {
                var selectedFolder = folderDialog.SelectedPath;
                viewModel.ReflexFolderPath = folderDialog.SelectedPath;
            }
        }

        private void ReflexFolderPathTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (App.IsValidReflexFolderPath(viewModel.ReflexFolderPath))
            {
                ReflexFolderPathTextBox.Background = new SolidColorBrush(Color.FromArgb(255, 200, 255, 200));
            }
            else
            {
                ReflexFolderPathTextBox.Background = new SolidColorBrush(Color.FromArgb(255, 255, 200, 200));
            }
        }

        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            var aboutDialog = new AboutWindow();
            aboutDialog.Owner = this;
            aboutDialog.ShowDialog();
        }

        private IList<string> GetFilter(string filterName, IEnumerable<string> filterValues, IEnumerable<string> currentSelectedFilterValues=null)
        {
            if (filterValues == null)
                return null;

            var filterItems = filterValues.Select(filterValue => {
                var isSelected = false;

                if (currentSelectedFilterValues != null)
                {
                    isSelected = currentSelectedFilterValues.Contains(filterValue);
                }

                return new FilterItem() { Value = filterValue, IsSelected = isSelected };
            }).ToList();

            filterItems = filterItems.OrderBy(filterItem => filterItem.Value).ToList();

            var filterDialog = new FilterSelectionWindow(filterName, filterItems);
            filterDialog.Owner = this;
            var result = filterDialog.ShowDialog();

            if ((bool)result)
            {
                var newFilter = new List<string>();

                foreach (var filterItem in filterDialog.FilterItems)
                {
                    if (filterItem.IsSelected)
                    {
                        newFilter.Add(filterItem.Value);
                    }
                }

                return newFilter;
            }
            else
            {
                return null;
            }
        }

        private void FilterPlayersForRecentOnlineReplaysButton_Click(object sender, RoutedEventArgs e)
        {
            var playerNames = viewModel.RecentOnlineReplays.SelectMany(replay => replay.Players).Select(replay => replay.Name).Distinct();

            var newFilter = GetFilter("Player", playerNames, viewModel.PlayersFilterForRecentOnlineReplays);

            if (newFilter != null)
            {
                viewModel.PlayersFilterForRecentOnlineReplays = newFilter;
            }
        }

        private void FilterMapsForRecentOnlineReplaysButton_Click(object sender, RoutedEventArgs e)
        {
            var mapNames = viewModel.RecentOnlineReplays.Select(replay => replay.Map).Distinct();

            var newFilter = GetFilter("Map", mapNames, viewModel.MapsFilterForRecentOnlineReplays);

            if (newFilter != null)
            {
                viewModel.MapsFilterForRecentOnlineReplays = newFilter;
            }
        }

        private void FilterGameModesForRecentOnlineReplaysButton_Click(object sender, RoutedEventArgs e)
        {
            var gameModes = viewModel.RecentOnlineReplays.Select(replay => replay.GameMode).Distinct();

            var newFilter = GetFilter("Game Mode", gameModes, viewModel.GameModesFilterForRecentOnlineReplays);

            if (newFilter != null)
            {
                viewModel.GameModesFilterForRecentOnlineReplays = newFilter;
            }
        }

        private void FilterPlayersForDownloadedReplaysButton_Click(object sender, RoutedEventArgs e)
        {
            var playerNames = viewModel.DownloadedReplays.SelectMany(replay => replay.Players).Select(replay => replay.Name).Distinct();

            var newFilter = GetFilter("Player", playerNames, viewModel.PlayersFilterForDownloadedReplays);

            if (newFilter != null)
            {
                viewModel.PlayersFilterForDownloadedReplays = newFilter;
            }
        }

        private void FilterMapsForDownloadedReplaysButton_Click(object sender, RoutedEventArgs e)
        {
            var mapNames = viewModel.DownloadedReplays.Select(replay => replay.Map).Distinct();

            var newFilter = GetFilter("Map", mapNames, viewModel.MapsFilterForDownloadedReplays);

            if (newFilter != null)
            {
                viewModel.MapsFilterForDownloadedReplays = newFilter;
            }
        }

        private void FilterGameModesForDownloadedReplaysButton_Click(object sender, RoutedEventArgs e)
        {
            var gameModes = viewModel.DownloadedReplays.Select(replay => replay.GameMode).Distinct();

            var newFilter = GetFilter("Game Mode", gameModes, viewModel.GameModesFilterForDownloadedReplays);

            if (newFilter != null)
            {
                viewModel.GameModesFilterForDownloadedReplays = newFilter;
            }
        }
    }
}
