﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflexReplayHelper
{
    public class Utilities
    {
        public static Replay ReplayFromStream(Stream stream)
        {
            var replay = new Replay();

            using (BinaryReader br = new BinaryReader(stream))
            {
                uint tag = br.ReadUInt32();
                //Console.WriteLine("Tag: " + tag.ToString());

                uint protocolVersion = br.ReadUInt32();
                //Console.WriteLine("Protocol Version: " + protocolVersion.ToString());

                uint playerCount = br.ReadUInt32();
                //Console.WriteLine("Player Count: " + playerCount.ToString());

                uint markerCount = br.ReadUInt32();
                replay.MarkerCount = markerCount;
                //Console.WriteLine("Marker Count: " + markerCount.ToString());

                uint gameFinished = br.ReadUInt32();
                //Console.WriteLine("Game Finished: " + gameFinished);

                uint padding = br.ReadUInt32();

                ulong workshopId = br.ReadUInt64();
                //Console.WriteLine("Workshop ID: " + workshopId);

                ulong epochStartTime = br.ReadUInt64();
                //Console.WriteLine("Epoch Start Time: " + epochStartTime.ToString());

                var gameModeAsCharArray = br.ReadChars(64);
                string gameMode = new string(gameModeAsCharArray).Trim('\0');
                replay.GameMode = gameMode;
                //Console.WriteLine("Game Mode: " + gameMode);

                string mapName = new string(br.ReadChars(256)).Trim('\0');
                replay.Map = mapName;
                //Console.WriteLine("Map Name: " + mapName);

                string hostName = new string(br.ReadChars(256)).Trim('\0');
                //Console.WriteLine("Host Name: " + hostName);

                for (int i = 0; i < playerCount; i++)
                {
                    var player = new Player();
                    //Console.WriteLine("");

                    string playerName = new string(br.ReadChars(32)).Trim(new char[] { '\u0002', '\u0003', '\u000E', '\0', '\t', '\n' });
                    player.Name = playerName;
                    //Console.WriteLine("Player Name: " + playerName);

                    int score = br.ReadInt32();
                    player.Score = score;
                    //Console.WriteLine("Score: " + score.ToString());

                    int team = br.ReadInt32();
                    if (team == 1)
                        player.Team = "B";
                    else
                        player.Team = "A";
                    //Console.WriteLine("Team: " + team.ToString());

                    ulong steamId = br.ReadUInt64();
                    //Console.WriteLine("Steam ID: " + steamId.ToString());

                    replay.Players.Add(player);
                }
            }

            return replay;
        }

        public static Replay ReplayFromFilePath(string filePath)
        {
            Replay replay = null;

            using (var replayFileStream = File.Open(filePath, FileMode.Open))
            {
                replay = ReplayFromStream(replayFileStream);
            }

            replay.FileName = Path.GetFileName(filePath);

            return replay;
        }
    }
}
