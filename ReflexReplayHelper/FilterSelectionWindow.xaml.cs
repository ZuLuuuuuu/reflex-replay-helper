﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ReflexReplayHelper
{
    public partial class FilterSelectionWindow : Window
    {
        private string filterName;
        private ObservableCollection<FilterItem> filterItems;

        public ObservableCollection<FilterItem> FilterItems
        {
            get
            {
                if ((bool)DontFilterCheckBox.IsChecked)
                    return new ObservableCollection<FilterItem>();
                else
                    return filterItems;
            }
        }

        public FilterSelectionWindow(string filterName, IList<FilterItem> filterItems)
        {
            InitializeComponent();

            this.filterName = filterName;

            this.filterItems = new ObservableCollection<FilterItem>();

            foreach (var filterItem in filterItems)
            {
                this.filterItems.Add(filterItem);
            }

            DataContext = this;

            Title = "Filter " + filterName + "s";
        }

        private void DontFilterCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)DontFilterCheckBox.IsChecked)
            {
                FilterItemsItemsControl.IsEnabled = false;
            }
            else
            {
                FilterItemsItemsControl.IsEnabled = true;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
