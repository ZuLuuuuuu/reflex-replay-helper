﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ReflexReplayHelper
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        
        private App app;
        ObservableCollection<ReplayViewModel> recentOnlineReplays;
        ObservableCollection<ReplayViewModel> downloadedReplays;
        IList<string> playersFilterForRecentOnlineReplays;
        IList<string> mapsFilterForRecentOnlineReplays;
        IList<string> gameModesFilterForRecentOnlineReplays;
        IList<string> playersFilterForDownloadedReplays;
        IList<string> mapsFilterForDownloadedReplays;
        IList<string> gameModesFilterForDownloadedReplays;
        uint minimumMarkerCountFilter = 0;
        bool refreshingRecentOnlineReplays;
        App.ReplayOrderType downloadedReplaysOrder;

        public ObservableCollection<ReplayViewModel> RecentOnlineReplays
        {
            get
            {
                return recentOnlineReplays;
            }
        }

        public ObservableCollection<ReplayViewModel> DownloadedReplays
        {
            get
            {
                return downloadedReplays;
            }
        }

        public ICollectionView RecentOnlineReplaysView
        {
            get
            {
                var view = CollectionViewSource.GetDefaultView(RecentOnlineReplays);

                view.Filter = replayObj => {
                    var playersFilterResult = false;
                    var mapsFilterResult = false;
                    var gameModesFilterResult = false;
                    var overallResult = false;

                    ReplayViewModel replay = replayObj as ReplayViewModel;

                    if (PlayersFilterForRecentOnlineReplays == null || PlayersFilterForRecentOnlineReplays.Count == 0)
                    {
                        playersFilterResult = true;
                    }
                    else
                    {
                        foreach (var player in replay.Players)
                        {
                            if (PlayersFilterForRecentOnlineReplays.Contains(player.Name))
                                playersFilterResult = true;
                        }
                    }

                    if (MapsFilterForRecentOnlineReplays == null || MapsFilterForRecentOnlineReplays.Count == 0)
                    {
                        mapsFilterResult = true;
                    }
                    else
                    {
                        if (MapsFilterForRecentOnlineReplays.Contains(replay.Map))
                            mapsFilterResult = true;
                    }

                    if (GameModesFilterForRecentOnlineReplays == null || GameModesFilterForRecentOnlineReplays.Count == 0)
                    {
                        gameModesFilterResult = true;
                    }
                    else
                    {
                        if (GameModesFilterForRecentOnlineReplays.Contains(replay.GameMode))
                            gameModesFilterResult = true;
                    }

                    overallResult = playersFilterResult && mapsFilterResult && gameModesFilterResult;

                    return overallResult;
                };

                return view;
            }
        }

        public ICollectionView DownloadedReplaysView
        {
            get
            {
                var view = CollectionViewSource.GetDefaultView(DownloadedReplays);

                view.Filter = replayObj => {
                    var playersFilterResult = false;
                    var mapsFilterResult = false;
                    var gameModesFilterResult = false;
                    var minimumMarkerCountFilterResult = false;
                    var overallResult = false;

                    ReplayViewModel replay = replayObj as ReplayViewModel;

                    if (PlayersFilterForDownloadedReplays == null || PlayersFilterForDownloadedReplays.Count == 0)
                    {
                        playersFilterResult = true;
                    }
                    else
                    {
                        foreach (var player in replay.Players)
                        {
                            if (PlayersFilterForDownloadedReplays.Contains(player.Name))
                                playersFilterResult = true;
                        }
                    }

                    if (MapsFilterForDownloadedReplays == null || MapsFilterForDownloadedReplays.Count == 0)
                    {
                        mapsFilterResult = true;
                    }
                    else
                    {
                        if (MapsFilterForDownloadedReplays.Contains(replay.Map))
                            mapsFilterResult = true;
                    }

                    if (GameModesFilterForDownloadedReplays == null || GameModesFilterForDownloadedReplays.Count == 0)
                    {
                        gameModesFilterResult = true;
                    }
                    else
                    {
                        if (GameModesFilterForDownloadedReplays.Contains(replay.GameMode))
                            gameModesFilterResult = true;
                    }

                    if (replay.MarkerCount >= minimumMarkerCountFilter)
                    {
                        minimumMarkerCountFilterResult = true;
                    }

                    overallResult = playersFilterResult && mapsFilterResult && gameModesFilterResult && minimumMarkerCountFilterResult;

                    return overallResult;
                };

                return view;
            }
        }

        public IList<string> PlayersFilterForRecentOnlineReplays
        {
            get
            {
                return playersFilterForRecentOnlineReplays;
            }

            set
            {
                playersFilterForRecentOnlineReplays = value;

                NotifyPropertyChanged(nameof(RecentOnlineReplays));
                NotifyPropertyChanged(nameof(RecentOnlineReplaysView));
                NotifyPropertyChanged(nameof(PlayersFilterForRecentOnlineReplaysValuesAsString));
            }
        }

        public IList<string> PlayersFilterForDownloadedReplays
        {
            get
            {
                return playersFilterForDownloadedReplays;
            }

            set
            {
                playersFilterForDownloadedReplays = value;

                NotifyPropertyChanged(nameof(DownloadedReplays));
                NotifyPropertyChanged(nameof(DownloadedReplaysView));
                NotifyPropertyChanged(nameof(PlayersFilterForDownloadedReplaysValuesAsString));
            }
        }

        public string PlayersFilterForRecentOnlineReplaysValuesAsString
        {
            get
            {
                if (playersFilterForRecentOnlineReplays == null || playersFilterForRecentOnlineReplays.Count == 0)
                {
                    return "Show All";
                }
                else
                {
                    return string.Join(", ", playersFilterForRecentOnlineReplays);
                }
            }
        }

        public string PlayersFilterForDownloadedReplaysValuesAsString
        {
            get
            {
                if (playersFilterForDownloadedReplays == null || playersFilterForDownloadedReplays.Count == 0)
                {
                    return "Show All";
                }
                else
                {
                    return string.Join(", ", playersFilterForDownloadedReplays);
                }
            }
        }

        public IList<string> MapsFilterForRecentOnlineReplays
        {
            get
            {
                return mapsFilterForRecentOnlineReplays;
            }

            set
            {
                mapsFilterForRecentOnlineReplays = value;

                NotifyPropertyChanged(nameof(RecentOnlineReplays));
                NotifyPropertyChanged(nameof(RecentOnlineReplaysView));
                NotifyPropertyChanged(nameof(MapsFilterForRecentOnlineReplaysValuesAsString));
            }
        }

        public IList<string> MapsFilterForDownloadedReplays
        {
            get
            {
                return mapsFilterForDownloadedReplays;
            }

            set
            {
                mapsFilterForDownloadedReplays = value;

                NotifyPropertyChanged(nameof(DownloadedReplays));
                NotifyPropertyChanged(nameof(DownloadedReplaysView));
                NotifyPropertyChanged(nameof(MapsFilterForDownloadedReplaysValuesAsString));
            }
        }

        public string MapsFilterForRecentOnlineReplaysValuesAsString
        {
            get
            {
                if (mapsFilterForRecentOnlineReplays == null || mapsFilterForRecentOnlineReplays.Count == 0)
                {
                    return "Show All";
                }
                else
                {
                    return string.Join(", ", mapsFilterForRecentOnlineReplays);
                }
            }
        }

        public string MapsFilterForDownloadedReplaysValuesAsString
        {
            get
            {
                if (mapsFilterForDownloadedReplays == null || mapsFilterForDownloadedReplays.Count == 0)
                {
                    return "Show All";
                }
                else
                {
                    return string.Join(", ", mapsFilterForDownloadedReplays);
                }
            }
        }

        public IList<string> GameModesFilterForRecentOnlineReplays
        {
            get
            {
                return gameModesFilterForRecentOnlineReplays;
            }

            set
            {
                gameModesFilterForRecentOnlineReplays = value;

                NotifyPropertyChanged(nameof(RecentOnlineReplays));
                NotifyPropertyChanged(nameof(RecentOnlineReplaysView));
                NotifyPropertyChanged(nameof(GameModesFilterForRecentOnlineReplaysValuesAsString));
            }
        }

        public IList<string> GameModesFilterForDownloadedReplays
        {
            get
            {
                return gameModesFilterForDownloadedReplays;
            }

            set
            {
                gameModesFilterForDownloadedReplays = value;

                NotifyPropertyChanged(nameof(DownloadedReplays));
                NotifyPropertyChanged(nameof(DownloadedReplaysView));
                NotifyPropertyChanged(nameof(GameModesFilterForDownloadedReplaysValuesAsString));
            }
        }

        public string GameModesFilterForRecentOnlineReplaysValuesAsString
        {
            get
            {
                if (gameModesFilterForRecentOnlineReplays == null || gameModesFilterForRecentOnlineReplays.Count == 0)
                {
                    return "Show All";
                }
                else
                {
                    return string.Join(", ", gameModesFilterForRecentOnlineReplays);
                }
            }
        }

        public string GameModesFilterForDownloadedReplaysValuesAsString
        {
            get
            {
                if (gameModesFilterForDownloadedReplays == null || gameModesFilterForDownloadedReplays.Count == 0)
                {
                    return "Show All";
                }
                else
                {
                    return string.Join(", ", gameModesFilterForDownloadedReplays);
                }
            }
        }

        public uint MinimumMarkerCountFilterForDownloadedReplays
        {
            get
            {
                return minimumMarkerCountFilter;
            }

            set
            {
                minimumMarkerCountFilter = value;

                NotifyPropertyChanged(nameof(DownloadedReplays));
                NotifyPropertyChanged(nameof(DownloadedReplaysView));
            }
        }

        public App.ReplayOrderType DownloadedReplaysOrder
        {
            get
            {
                return downloadedReplaysOrder;
            }

            set
            {
                downloadedReplaysOrder = value;

                RefreshDownloadedReplays();
                
                NotifyPropertyChanged(nameof(DownloadedReplaysOrder));
            }
        }

        public bool RefreshingRecentOnlineReplays
        {
            get
            {
                return refreshingRecentOnlineReplays;
            }

            set
            {
                refreshingRecentOnlineReplays = value;

                NotifyPropertyChanged(nameof(RefreshingRecentOnlineReplays));
            }
        }

        public string ReflexFolderPath
        {
            get
            {
                return app.ReflexFolderPath;
            }

            set
            {
                app.ReflexFolderPath = value;

                NotifyPropertyChanged(nameof(ReflexFolderPath));
            }
        }

        public string FilterSteamId
        {
            get
            {
                return app.FilterSteamId;
            }

            set
            {
                app.FilterSteamId = value;

                NotifyPropertyChanged(nameof(FilterSteamId));
            }
        }

        public MainWindowViewModel()
        {
            app = (App)App.Current;
            recentOnlineReplays = new ObservableCollection<ReplayViewModel>();
            downloadedReplays = new ObservableCollection<ReplayViewModel>();
        }

        public void RefreshDownloadedReplays()
        {
            var downloadedReplays = app.GetDownloadedReplays(order: DownloadedReplaysOrder);

            DownloadedReplays.Clear();

            foreach (var replay in downloadedReplays)
            {
                DownloadedReplays.Add(new ReplayViewModel(replay) { Downloaded = true });
            }
        }

        public async Task RefreshRecentOnlineReplays()
        {
            RefreshingRecentOnlineReplays = true;

            var recentOnlineReplays = await app.GetRecentOnlineReplays();

            RecentOnlineReplays.Clear();

            foreach (var replay in recentOnlineReplays)
            {
                var downloaded = false;
                if (File.Exists(Path.Combine(app.ReflexFolderPath, "replays", ReplayViewModel.ReplayFileNameFromUrl(replay.Url))))
                {
                    downloaded = true;
                }
                RecentOnlineReplays.Add(new ReplayViewModel(replay) { Downloaded = downloaded });
            }

            RefreshingRecentOnlineReplays = false;
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
