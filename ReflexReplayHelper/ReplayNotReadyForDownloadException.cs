﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflexReplayHelper
{
    public class ReplayNotReadyForDownloadException : Exception
    {
        public ReplayNotReadyForDownloadException() : base("Replay is not ready for download right now, please try it again in a minute.")
        {
        }

        public ReplayNotReadyForDownloadException(string message) : base(message)
        {
        }
    }
}
